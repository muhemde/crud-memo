package com.example.crudmemo.Data.Source

import com.example.crudmemo.Data.Model.TaskEntity

interface TaskRepositoryInterface {

    fun insertTask(taskEntity: TaskEntity) : Long?

    fun getAllTask() : List<TaskEntity>?

    fun updateTask(taskEntity: TaskEntity) : Int?

    fun deleteTask(taskEntity: TaskEntity) : Int?
}