package com.example.crudmemo.Data.Source.Local.DAO

import androidx.room.*
import com.example.crudmemo.Data.Model.TaskEntity

@Dao
interface TaskDao {

    @Insert
    fun insertTask(taskEntity: TaskEntity) : Long

    @Query("SELECT * FROM TaskEntity")
    fun getAllTask() : List<TaskEntity>

    @Update
    fun updateTask(taskEntity: TaskEntity) : Int

    @Delete
    fun deleteTask(taskEntity: TaskEntity) : Int

}