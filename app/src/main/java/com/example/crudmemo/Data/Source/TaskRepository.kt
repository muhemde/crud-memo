package com.example.crudmemo.Data.Source

import android.content.Context
import com.example.crudmemo.Data.Model.TaskEntity
import com.example.crudmemo.Data.Source.Local.DatabaseManager

class TaskRepository(context: Context) : TaskRepositoryInterface {

    private var mDatabaseManager : DatabaseManager? =null
    init {
        mDatabaseManager = DatabaseManager.getInstance(context)
    }

    override fun insertTask(taskEntity: TaskEntity): Long? {
        return mDatabaseManager?.taskDao()?.insertTask(taskEntity)
    }

    override fun getAllTask(): List<TaskEntity>? {
        return mDatabaseManager?.taskDao()?.getAllTask()
    }

    override fun updateTask(taskEntity: TaskEntity): Int? {
        return mDatabaseManager?.taskDao()?.updateTask(taskEntity)
    }

    override fun deleteTask(taskEntity: TaskEntity): Int? {
        return mDatabaseManager?.taskDao()?.deleteTask(taskEntity)
    }
}