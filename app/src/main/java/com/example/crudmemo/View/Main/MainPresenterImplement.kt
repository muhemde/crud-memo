package com.example.crudmemo.View.Main

import com.example.crudmemo.Data.Model.TaskEntity
import com.example.crudmemo.Data.Source.TaskRepositoryInterface

class MainPresenterImplement(private val mainView: MainView,
                             private val taskRepositoryInterface: TaskRepositoryInterface) :
    MainPresenter {
    override fun getAllTask() {
        val result = taskRepositoryInterface.getAllTask()

        mainView.getAllTask(result)
    }

    override fun deleteTask(taskEntity: TaskEntity) {
        val result = taskRepositoryInterface.deleteTask(taskEntity)

        if (result != 0){
            mainView.onDelete("Delete Success")
        } else {
            mainView.onDelete("Delete Failed")
        }
    }

}