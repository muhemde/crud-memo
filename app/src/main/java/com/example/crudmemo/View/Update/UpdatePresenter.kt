package com.example.crudmemo.View.Update

import com.example.crudmemo.Data.Model.TaskEntity

interface UpdatePresenter {

    fun updateTask(taskEntity: TaskEntity)

}