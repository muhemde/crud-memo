package com.example.crudmemo.View.Update

import com.example.crudmemo.Data.Model.TaskEntity
import com.example.crudmemo.Data.Source.TaskRepositoryInterface

class UpdatePresenterImplement(private val updateView: UpdateView,
                               private val taskRepositoryInterface: TaskRepositoryInterface) :
    UpdatePresenter {

    override fun updateTask(taskEntity: TaskEntity) {

        val result = taskRepositoryInterface.updateTask(taskEntity)

        if (result != 0){
            updateView.onSuccessEdit("Success Update Data")
        } else {
            updateView.onSuccessEdit("Failed Update Data")
        }
    }
}