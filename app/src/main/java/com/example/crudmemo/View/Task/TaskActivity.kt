package com.example.crudmemo.View.Task

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crudmemo.Data.Model.TaskEntity
import com.example.crudmemo.Data.Source.TaskRepository
import com.example.crudmemo.Data.Source.TaskRepositoryInterface
import com.example.crudmemo.R
import com.example.crudmemo.View.Main.MainSecondActivity
import kotlinx.android.synthetic.main.activity_task.*

class TaskActivity : AppCompatActivity(),TaskView {

    lateinit var taskRepositoryInterface : TaskRepositoryInterface
    lateinit var taskPresenter: TaskPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)

        taskRepositoryInterface = TaskRepository(this)
        taskPresenter = TaskPresenterImplement(this,taskRepositoryInterface)
        val list : View = fabListTask

        list.setOnClickListener {

            val task = Intent(this@TaskActivity, MainSecondActivity::class.java)
            startActivity(task)

        }
    }


    override fun onSuccessInsert(result: String) {

        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        etTitleTask.setText("")
        etDescriptionTask.setText("")

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task,menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){

            R.id.saveDrive ->{

                val title = etTitleTask.text.toString()
                val description = etDescriptionTask.text.toString()
                taskPresenter.insertTask(TaskEntity(null,title,description))
                true
            }


            else ->  return super.onOptionsItemSelected(item)

        }

    }
}