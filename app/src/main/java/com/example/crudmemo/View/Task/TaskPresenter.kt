package com.example.crudmemo.View.Task

import com.example.crudmemo.Data.Model.TaskEntity

interface TaskPresenter {

     fun insertTask(taskEntity: TaskEntity)

}