package com.example.crudmemo.View.Main

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.crudmemo.View.Task.TaskActivity
import com.example.crudmemo.Data.Model.TaskEntity
import com.example.crudmemo.Data.Source.TaskRepository
import com.example.crudmemo.Data.Source.TaskRepositoryInterface
import com.example.crudmemo.R
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainSecondActivity : AppCompatActivity(), MainView {

    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var mainPresenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        taskRepositoryInterface = TaskRepository(this)
        mainPresenter = MainPresenterImplement(this, taskRepositoryInterface)
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        mainPresenter.getAllTask()

        swipeRefresh.setOnRefreshListener{

            mainPresenter.getAllTask()
            swipeRefresh.isRefreshing = false

        }

        val add : View = addFloatingButton
        add.setOnClickListener {
            val fabButton = Intent(this@MainSecondActivity, TaskActivity::class.java)
            startActivity(fabButton)

        }
    }

    override fun getAllTask(list: List<TaskEntity>?) {
        GlobalScope.launch {

            runOnUiThread {

                list?.let{

                    val mainAdapter = MainAdapter(it, mainPresenter)
                    recyclerView.adapter = mainAdapter

                }
            }
        }
    }

    override fun onDelete(result: String) {

        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()

    }
}