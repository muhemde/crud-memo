package com.example.crudmemo.View.Update

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.crudmemo.Data.Model.TaskEntity
import com.example.crudmemo.Data.Source.TaskRepository
import com.example.crudmemo.Data.Source.TaskRepositoryInterface
import com.example.crudmemo.R
import com.example.crudmemo.View.Main.MainSecondActivity
import com.example.crudmemo.View.Task.TaskActivity
import kotlinx.android.synthetic.main.activity_edit.*

class UpdateActivity : AppCompatActivity(), UpdateView {

    lateinit var taskRepositoryInterface: TaskRepositoryInterface
    lateinit var updatePresenter: UpdatePresenter
    private val Task : TaskEntity? by lazy{

        intent.getParcelableExtra<TaskEntity>("Task")

    }

    override fun onSuccessEdit(result: String) {

        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
        etTitleEdit.setText("")
        etDescriptionEdit.setText("")

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        taskRepositoryInterface = TaskRepository(this)
        updatePresenter = UpdatePresenterImplement(this, taskRepositoryInterface)
        Task?.let {

            etTitleEdit.setText(it.title)
            etDescriptionEdit.setText(it.description)

        }

        val listEdit : View = fabEdit

        //For Edit
        listEdit.setOnClickListener {

            val update = Intent(this@UpdateActivity, MainSecondActivity::class.java)
            startActivity(update)

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        val inflater : MenuInflater = menuInflater
        inflater.inflate(R.menu.item_task, menu)
        return true

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when(item.itemId){

            R.id.saveDrive -> {

                val title = etTitleEdit.text.toString()
                val description = etDescriptionEdit.text.toString()

                updatePresenter.updateTask(TaskEntity(Task?.id, title, description))
                true

            }

            else -> super.onOptionsItemSelected(item)

        }
    }

}