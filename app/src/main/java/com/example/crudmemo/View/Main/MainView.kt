package com.example.crudmemo.View.Main

import com.example.crudmemo.Data.Model.TaskEntity

interface MainView {

    fun getAllTask(list: List<TaskEntity>?)

    fun onDelete(result : String)
}