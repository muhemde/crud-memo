package com.example.crudmemo.View.Main

import com.example.crudmemo.Data.Model.TaskEntity

interface MainPresenter {
    fun getAllTask()

    fun deleteTask(taskEntity: TaskEntity)
}