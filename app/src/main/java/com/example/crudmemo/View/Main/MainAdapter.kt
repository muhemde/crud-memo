package com.example.crudmemo.View.Main

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.crudmemo.View.Update.UpdateActivity
import com.example.crudmemo.Data.Model.TaskEntity
import com.example.crudmemo.R
import kotlinx.android.synthetic.main.list_view_task.view.*

class MainAdapter (private val listTask : List<TaskEntity>,
                   private val mainPresenter: MainPresenter) : RecyclerView.Adapter<MainAdapter.ListTaskViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListTaskViewHolder {
        return ListTaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_view_task, parent, false))
    }

    override fun onBindViewHolder(holder: ListTaskViewHolder, position: Int) {
        listTask[position].let {
            holder.title.text = it.title
            holder.description.text = it.description
        }

        holder.itemView.btnEdit.setOnClickListener {
            val intentEditActivity = Intent(it.context, UpdateActivity::class.java)

            intentEditActivity.putExtra("Task", listTask[position])
            it.context.startActivity((intentEditActivity))
        }

        holder.itemView.btnDelete.setOnClickListener{
            AlertDialog.Builder(it.context).setPositiveButton("Yes"){p0, p1 ->
                mainPresenter.deleteTask(listTask[position])
            }.setNegativeButton("No"){ p0, p1 ->
                p0.dismiss()
            }.setMessage("Delete Data ${listTask[position].id}?").setTitle("Confirm").create().show()
        }
    }

    override fun getItemCount(): Int {
        return listTask.size
    }

    inner class ListTaskViewHolder(view: View) : RecyclerView.ViewHolder(view){
        val title: TextView = view.tvTitle
        val description: TextView = view.tvDescription
    }
}